import json
import os
import psycopg2
import datetime
import decimal

from django.db import connection
from django.core.context_processors import csrf

def dictfetchall(cursor):
    "Returns all rows from a cursor as a dict"
    desc = cursor.description
    return [
        dict(zip([col[0] for col in desc], row))
        for row in cursor.fetchall()
    ]

def get_all_events() -> list((
        "value"
    )):
    cursor = connection.cursor()
    cursor.execute("select ser.tiposensor, lei.valor, lei.dataleitura from sensor ser inner join leitura lei on ser.idsensor = lei.idsensor")
    result = dictfetchall(cursor)

    return result