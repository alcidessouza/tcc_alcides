import datetime
import redis
import json
import time

from datetime import datetime
from datetime import date
from json import dumps
from django.core.context_processors import csrf
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_exempt
from django.db import connection

redis = redis.Redis(unix_socket_path="/job/wmon/run/redis/wmon_dev.sock")

def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, datetime):
        serial = obj.isoformat()
        return serial

def ajax_temp(request):
    cursor = connection.cursor()
    cursor.execute("select valor from leitura where idsensor = 1 order by idleitura desc limit 1;")
    result_temp = dictfetchall(cursor)
    return HttpResponse(result_temp)

def ajax_umid(request):
    cursor = connection.cursor()
    cursor.execute("select valor from leitura where idsensor = 2 order by idleitura desc limit 1;")
    result_umid = dictfetchall(cursor)
    return HttpResponse(result_umid)

def ajax_fuma(request):
    cursor = connection.cursor()
    cursor.execute("select valor from leitura where idsensor = 3 order by idleitura desc limit 1;")
    result_fuma = dictfetchall(cursor)
    return HttpResponse(result_fuma)

def home(request):
    return render_to_response("home/main.html", {
        'csrf_token': csrf(request)['csrf_token'],
    })

def consultas(request):
    return render_to_response("home/consultas.html", {
        'csrf_token': csrf(request)['csrf_token'],
    })

def sobre(request):
    return render_to_response("home/sobre.html", {
        'csrf_token': csrf(request)['csrf_token'],
    })

def contato(request):
    return render_to_response("home/contato.html", {
        'csrf_token': csrf(request)['csrf_token'],
    })

def dictfetchall(cursor):
    "Returns all rows from a cursor as a dict"
    desc = cursor.description
    return [
        dict(zip([col[0] for col in desc], row))
        for row in cursor.fetchall()
    ]

def fmt_data(num):
    if num < 10:
        return '0' + str(num)
    else:
        return str(num)

def hora_local():
    agora = datetime.now()

    dia = fmt_data(agora.day)
    mes = fmt_data(agora.month)
    ano = fmt_data(agora.year)
    hor = fmt_data(agora.hour)
    seg = fmt_data(agora.minute)
    min = fmt_data(agora.second)

    return(str(dia) + "/" + str(mes) + "/" + str(ano) + " " + str(hor) + ":" + str(min) + ":" + str(seg))

# def graph_view(request, temperatura, umidade, fumaca, dataini, datafim):
def graph_view(request):
    cursor = connection.cursor()

    dataini = request.POST['dataini']
    dataaux = datetime.strptime(dataini , '%Y-%m-%d')
    dataini = str(dataaux.year) + '-' + str(('%02d' % dataaux.month)) + '-' +  str(('%02d' % dataaux.day))

    datafim = request.POST['datafim']
    dataaux = datetime.strptime(datafim , '%Y-%m-%d')
    datafim = str(dataaux.year) + '-' + str(('%02d' % dataaux.month)) + '-' +  str(('%02d' % dataaux.day))

    data_temp = []
    data_umid = []
    data_fuma = []

    for x in request.POST:
        if x == 'temp':
            cursor.execute("select ser.tiposensor, lei.valor, lei.dataleitura from sensor ser inner join leitura lei on ser.idsensor = lei.idsensor where ser.idsensor = 1 and lei.dataleitura between '" + dataini + " 00:00:00' and '" + datafim + " 23:59:59' order by lei.dataleitura;")
            result_temp = dictfetchall(cursor)

            data_temp = []
            for x in result_temp:
                tiposensor = x["tiposensor"]
                valor = x["valor"]
                dataleitura = dumps(x["dataleitura"], default=json_serial)
                dataleitura = dataleitura.replace("T"," ")
                dataleitura = dataleitura.replace('"',"")
                data_temp.append( {tiposensor:valor, "date":dataleitura} )
            json.dumps(data_temp)
        elif x == 'umid':
            cursor.execute("select ser.tiposensor, lei.valor, lei.dataleitura from sensor ser inner join leitura lei on ser.idsensor = lei.idsensor where ser.idsensor = 2 and lei.dataleitura between '" + dataini + " 00:00:00' and '" + datafim + " 23:59:59' order by lei.dataleitura;")
            result_umid = dictfetchall(cursor)

            data_umid = []
            for x in result_umid:
                tiposensor = x["tiposensor"]
                valor = x["valor"]
                dataleitura = dumps(x["dataleitura"], default=json_serial)
                dataleitura = dataleitura.replace("T"," ")
                dataleitura = dataleitura.replace('"',"")
                data_umid.append( {tiposensor:valor, "date":dataleitura} )
            json.dumps(data_umid)
        elif x == 'fuma':
            cursor.execute("select ser.tiposensor, lei.valor, lei.dataleitura from sensor ser inner join leitura lei on ser.idsensor = lei.idsensor where ser.idsensor = 3 and lei.dataleitura between '" + dataini + "' and '" + datafim + "';")
            result_fuma = dictfetchall(cursor)

            data_fuma = []
            for x in result_fuma:
                tiposensor = x["tiposensor"]
                valor = x["valor"]
                dataleitura = dumps(x["dataleitura"], default=json_serial)
                dataleitura = dataleitura.replace("T"," ")
                dataleitura = dataleitura.replace('"',"")
                data_fuma.append( {tiposensor:valor, "date":dataleitura} )
            json.dumps(data_fuma)

    return render_to_response("home/graficos.html", {'csrf_token':csrf(request)['csrf_token'], 
        'data_temp':data_temp,
        'data_umid':data_umid,
        'data_fuma':data_fuma,
    })





