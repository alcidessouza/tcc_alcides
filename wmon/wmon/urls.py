from django.conf.urls import patterns, include, url
from django.contrib import admin

from . import views
from . import graph

urlpatterns = patterns('',
	url(r'^home/$', views.home),
	url(r'^consultas/$', views.consultas),
	url(r'^sobre/$', views.sobre),
	url(r'^contato/$', views.contato),
    url(r'^graph$', views.graph_view),
    url(r'^data_temp$', views.ajax_temp),
    url(r'^data_umid$', views.ajax_umid),
    url(r'^data_fuma$', views.ajax_fuma),
    url(r'^admin/', include(admin.site.urls)),
)
