﻿<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<link type="text/css" rel="stylesheet" href="stylesheet2.css"/>
		<title>TCC-II</title>
	</head>
	
	<body>
		<script src="/d3.min.js"></script>
		<script src="/graficod3.js"></script>

 		<div id="header">
			<h1 id="name">SISTEMA DE MONITORAMENTO CLIM&Aacute;TICO USANDO SENSORES</h1>
		</div>
 		<div id="subheader">
			<table id="sidemenu">
				<tr>
					<td align="center" id="logo"><a href="http://www.cct.udesc.br/"><img src="http://oi60.tinypic.com/f4fer5.jpg"></a></td>
					<td align="center" id="sidemenutd"><a href="http://www.monitoramento.zz.vc//home2.html">Home</a></td>
					<td align="center" id="sidemenutd"><a href="http://www.monitoramento.zz.vc//consultas.html">Consultas</a></td>
					<td align="center" id="sidemenutd"><a href="http://www.monitoramento.zz.vc//sobre.html">Sobre o trabalho</a></td>
					<td align="center" id="sidemenutd"><a href="http://www.monitoramento.zz.vc//contato.html">Contato</a></td>
				</tr>				
			</table>
		</div>
		<div class="center" id="graph_area"></div>
		<?php
	
			//Aqui colocamos o servidor em que está o nosso banco de dados, no nosso exemplo é a conexão com um servidor local, portanto localhost 
			$servidor = "mysql.hostinger.com.br"; 
			
			//Aqui é o nome de usuário do seu banco de dados, root é o servidor inicial e básico de todo servidor, mas recomenda-se não usar o usuario root e sim criar um novo usuário 
			$usuario = "u549927703_tcc"; 

			//Aqui colocamos a senha do usuário, por padrão o usuário root vem sem senha, mas é altamente recomendável criar uma senha para o usuário root, visto que ele é o que tem mais privilégios no servidor 
			$senha = "tcctcc"; 
			
			$db_database= 'u549927703_tcc'; // o nome do banco de dados que iremos nos conectar.
			
			//Aqui criamos a conexão utilizando o servidor, usuario e senha, caso dê erro, retorna um erro ao usuário. 
			
			$conexao = mysql_connect($servidor, $usuario, $senha) or die ("Não foi possivel conectar ao servidor PostGreSQL"); 
			
			//caso a conexão seja efetuada com sucesso, exibe uma mensagem ao usuário echo "Conexão efetuada com sucesso!!";
			
			mysql_select_db($db_database, $conexao) or die (mysql_error());
			
			if(isset($_POST['Temperatura']))
			{
				$query_temp = "SELECT S.TIPOSENSOR, L.VALOR, L.DATALEITURA FROM SENSOR AS S INNER JOIN LEITURA AS L ON S.IDSENSOR = L.IDSENSOR WHERE S.IDSENSOR = 1 AND L.DATALEITURA BETWEEN '" . $_POST['dataini'] . "' AND '" . $_POST['datafim'] . "'";
			}
			if(isset($_POST['Umidade']))
			{
				$query_umid = "SELECT S.TIPOSENSOR, L.VALOR, L.DATALEITURA FROM SENSOR AS S INNER JOIN LEITURA AS L ON S.IDSENSOR = L.IDSENSOR WHERE S.IDSENSOR = 2 AND L.DATALEITURA BETWEEN '" . $_POST['dataini'] . "' AND '" . $_POST['datafim'] . "'";
			}
			if(isset($_POST['Fumaca']))
			{
				$query_fuma = "SELECT S.TIPOSENSOR, L.VALOR, L.DATALEITURA FROM SENSOR AS S INNER JOIN LEITURA AS L ON S.IDSENSOR = L.IDSENSOR WHERE S.IDSENSOR = 3 AND L.DATALEITURA BETWEEN '" . $_POST['dataini'] . "' AND '" . $_POST['datafim'] . "'";
			}

			// Realiza a consulta e guarda o resultado na váriavel $res
			$res = mysql_query($query_temp, $conexao);

			$contador = 0;
			$json_str = '[';
			while ($row = mysql_fetch_row($res)){
				if ($contador == 0){
					$json_str = $json_str . '{"' . $row[0] . '":"' . $row[1] . '", "date":"' . $row[2] . '"}';
				} else{
					$json_str = $json_str . ',{"' . $row[0] . '":"' . $row[1] . '", "date":"' . $row[2] . '"}';
				}
				$contador = $contador + 1;
			}
			$json_str = $json_str . ']';
		?>
		
		<script>
			var data = <?php echo $json_str; ?>;

			var br_BR = {
				"decimal": ".",
				"thousands": ",",
				"grouping": [3],
				"currency": ["$", ""],
				"dateTime": "%a %b %e %X %Y",
				"date": "%m/%d/%Y",
				"time": "%H:%M:%S",
				"periods": ["AM", "PM"],
				"days": ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"],
				"shortDays": ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
				"months": ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
				"shortMonths": ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"]
			}
			
			var BR = d3.locale(br_BR);

			var margin = {top: 20, right: 80, bottom: 30, left: 50},
				width = 1000 - margin.left - margin.right,
				height = 400 - margin.top - margin.bottom;

			var parseDate = d3.time.format("%Y-%m-%d %H:%M:%S").parse;

			var x = d3.time.scale()
				.range([0, width]);

			var y = d3.scale.linear()
				.range([height, 0]);

			var color = d3.scale.category10();

			var xAxis = d3.svg.axis()
				.scale(x)
				.orient("bottom")
				.tickFormat(BR.timeFormat("%H:%M:%S"));

			var yAxis = d3.svg.axis()
				.scale(y)
				.orient("left");

			var line = d3.svg.line()
				.interpolate("basis")
				.x(function(d) { return x(d.date); })
				.y(function(d) { return y(d.temperature); });

			var svg = d3.select("#graph_area").append("svg")
				.attr("width", width + margin.left + margin.right)
				.attr("height", height + margin.top + margin.bottom)
			  .append("g")
				.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

			color.domain(d3.keys(data[0]).filter(function(key) { return key !== "date"; }));

			data.forEach(function(d) {
				d.date = parseDate(String(d.date));
			});

			var cities = color.domain().map(function(name) {
				return {
					name: name,
					values: data.map(function(d) {
						return {date: d.date, temperature: +d[name]};
					})
				};
			});

			x.domain(d3.extent(data, function(d) { return d.date; }));

			y.domain([
				d3.min(cities, function(c) { return d3.min(c.values, function(v) { return v.temperature; }); }),
				d3.max(cities, function(c) { return d3.max(c.values, function(v) { return v.temperature; }); })
			]);

			svg.append("g")
				.attr("class", "x axis")
				.attr("transform", "translate(0," + height + ")")
				.call(xAxis);

			svg.append("g")
				.attr("class", "y axis")
				.call(yAxis)
				.append("text")
				.attr("transform", "rotate(-90)")
				.attr("y", 6)
				.attr("dy", ".71em")
				.style("text-anchor", "end")

			var city = svg.selectAll(".city")
				.data(cities)
				.enter().append("g")
				.attr("class", "city");

			city.append("path")
				.attr("class", "line")
				.attr("d", function(d) { return line(d.values); })
				.style("stroke", function(d) { return color(d.name); });

		</script>

	</body>
</html>
