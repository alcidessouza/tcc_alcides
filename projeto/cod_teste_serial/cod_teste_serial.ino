#include <SoftwareSerial.h>
#include <string.h>
#include "DHT.h"

// Pino digital em que foi conectado o DTH11
#define DHTPIN01 8
#define DHTTYPE DHT11 // DHT 11
DHT dht01 ( DHTPIN01, DHTTYPE );

// Define NewSoftSerial TX/RX pins
uint8_t ssRX = 2;
uint8_t ssTX = 3;
// Remember to connect all devices to a common Ground: XBee, Arduino and USB-Serial device
SoftwareSerial nss(ssRX, ssTX);

// Sensor value read from analog pin (range from 0 to 1023)
int iSensorValue = 0;
// Mapped value from 0 to 100
byte bySensorVal = 0;

void setup() {  
  Serial.begin(9600);
  nss.begin(9600);
  dht01.begin();
}

void loop() {
    // Le os valores do sensor dth11
    int h01 = dht01.readHumidity();
    int t01 = dht01.readTemperature();

    // Read input value on A0 and map it from 0 to 100
    iSensorValue = analogRead(A0);
    bySensorVal = map(iSensorValue, 0, 1023, 0, 100);
    
    nss.println("h" + String(h01));
    nss.println("t" + String(t01));
    nss.println("f" + String(bySensorVal));

    Serial.println("h" + String(h01));
    Serial.println("t" + String(t01));
    Serial.println("f" + String(bySensorVal));

    delay(30000);
}

