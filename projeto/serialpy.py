import time
import serial
import redis
 
ser = serial.Serial('/dev/ttyUSB0', 9600)
redis = redis.Redis('localhost')

while(True):
	pass

	# Pega os dados do Arduino (Ex: t24, h60, f0)
	leitura = ser.readline()
	codigo = leitura[0]

	# Salva os dados em listas
	if codigo == 't' or codigo == 'h' or codigo == 'f':
		redis.rpush('data',leitura)
	else:
		print("Erro de leitura")

	# Imprime a lista
	if redis.llen('data') > 0:
		print(redis.lrange('data', 0, -1))
