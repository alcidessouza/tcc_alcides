
import time
import serial
import redis
import requests
import psycopg2
import re

from datetime import datetime

ser = serial.Serial('/dev/ttyUSB0', 9600)
redis = redis.Redis(unix_socket_path="/job/wmon/run/redis/wmon_dev.sock")

def fmt_data(num):
    if num < 10:
        return '0' + str(num)
    else:
        return str(num)

def hora_local():
    agora = datetime.now()

    dia = fmt_data(agora.day)
    mes = fmt_data(agora.month)
    ano = fmt_data(agora.year)
    hor = fmt_data(agora.hour)
    seg = fmt_data(agora.minute)
    min = fmt_data(agora.second)

    return(str(dia) + "/" + str(mes) + "/" + str(ano) + " " + str(hor) + ":" + str(seg) + ":" + str(min))

def get_leitura():
    leitura = ser.readline()
    print(leitura)
    leitura = leitura.decode(encoding='UTF-8')
    return leitura

def save_redis(leitura, codigo):
    if codigo == 't' or codigo == 'h' or codigo == 'f':
        redis.rpush('data',leitura)
        redis.rpush('data',codigo + hora_local())
    else:
        print("Erro de leitura")

while(True):
    pass

    #Inicia as variáveis
    leitura = get_leitura()
    codigo = leitura[0]
    
    #Salva dados no redis
    save_redis(leitura, codigo)
    
    #Tenta conexão com postgre
    try:
        conn = psycopg2.connect("dbname='tcc2014' user='cid2014' host='s01.evn.com.br' port='5434' password='nYzTTxtugueDSJRA8oFMPx4xuB6PKh19'")
        conn.autocommit = True
        cur = conn.cursor()
        
        #Carrega chaves do redis e persiste no postgre
        if redis.llen('data') > 0:
            for row in redis.lrange('data', 0, -1):
                row = row.decode(encoding='UTF-8')
                row = row.replace("\n","")
                row = row.replace("\r","")

                cod = row[0]
                val = row[1:]
                dataleitura = hora_local()

                validator_val = re.compile(r'^\d{1,3}$')
                validator_dta = re.compile(r'^\d{1,2}/\d{1,2}/\d{1,4} \d{1,2}:\d{1,2}:\d{1,2}$')

                #Salva dados no postgree
                if validator_val.findall(val) and validator_dta.findall(dataleitura):
                    if cod == 't':
                        cur.execute("insert into leitura (idsensor, valor, dataleitura) values ( 1, " + val + ", '" + hora_local() + "');")
                    elif cod == 'h':
                        cur.execute("insert into leitura (idsensor, valor, dataleitura) values ( 2, " + val + ", '" + hora_local() + "');")
                    elif cod == 'f':
                        cur.execute("insert into leitura (idsensor, valor, dataleitura) values ( 3, " + val + ", '" + hora_local() + "');")

        # # Elimina chaves do redis
        redis.flushall()

        print("Commit")
    except:
        print("I am unable to connect to the postgree")
    time.sleep(2)